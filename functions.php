<?php
/**
 * Abalone functions and definitions
 *
 * @package Abalone
 */

/**
 * Set the content width based on the theme's design and stylesheet.
 */
if ( ! isset( $content_width ) ) {
	$content_width = 640; /* pixels */
}

if ( ! function_exists( 'abalone_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function abalone_setup() {

	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on Abalone, use a find and replace
	 * to change 'abalone' to the name of your theme in all the template files
	 */
	load_theme_textdomain( 'abalone', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link http://codex.wordpress.org/Function_Reference/add_theme_support#Post_Thumbnails
	 */
	//add_theme_support( 'post-thumbnails' );

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		'primary' => __( 'Primary Menu', 'abalone' ),
	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form', 'comment-form', 'comment-list', 'gallery', 'caption',
	) );

	/*
	 * Enable support for Post Formats.
	 * See http://codex.wordpress.org/Post_Formats
	 */
	add_theme_support( 'post-formats', array(
		'aside', 'image', 'video', 'quote', 'link',
	) );

	// Setup the WordPress core custom background feature.
	add_theme_support( 'custom-background', apply_filters( 'abalone_custom_background_args', array(
		'default-color' => 'ffffff',
		'default-image' => '',
	) ) );
}
endif; // abalone_setup
add_action( 'after_setup_theme', 'abalone_setup' );

/**
 * Register widget area.
 *
 * @link http://codex.wordpress.org/Function_Reference/register_sidebar
 */
function abalone_widgets_init() {
	register_sidebar( array(
		'name'          => __( 'Sidebar', 'abalone' ),
		'id'            => 'sidebar-1',
		'description'   => '',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h1 class="widget-title">',
		'after_title'   => '</h1>',
	) );
}
add_action( 'widgets_init', 'abalone_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function abalone_scripts() {
	wp_enqueue_style( 'abalone-style', get_stylesheet_uri() );

	wp_enqueue_style( 'normalize', get_template_directory_uri() . '/css/normalize', array(), '3.0.1', 'all');
	wp_enqueue_style( 'boostrap', get_template_directory_uri() . '/css/bootstrap.min.css', array(), '3.2.0', 'all' );
	wp_enqueue_style( 'foundation', get_template_directory_uri() . '/css/foundation.min.css', array(), '5.0.0', 'all' );

	wp_enqueue_script( 'boostrap', get_template_directory_uri() . '/js/bootstrap.min.js', array('jquery'), '3.2.0', true );
	wp_enqueue_script( 'abalone-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20120206', true );
	wp_enqueue_script( 'abalone-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20130115', true );
	wp_enqueue_script( 'foundation', get_template_directory_uri() . '/js/foundation.min.js', array(), '5.0.0', true );	
	wp_enqueue_script( 'foundation.offcanvas', get_template_directory_uri() . '/js/foundation.offcanvas.js', array(), '', true );	
	wp_enqueue_script( 'foundation.interchange', get_template_directory_uri() . '/js/foundation.interchange.js', array(), '', true );	
	wp_enqueue_script( 'script', get_template_directory_uri() . '/js/script.js', array(), '', true );	

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'abalone_scripts' );

/**
 * Implement the Custom Header feature.
 */
//require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
require get_template_directory() . '/inc/jetpack.php';

/**
 * change home menu text in wp_nav_menu();
 */
function page_menu_args( $args ) {
    $args['show_home'] = 'Home';
    return $args;
}
add_filter( 'wp_page_menu_args', 'page_menu_args' );






