<?php
/**
 * The header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package Abalone
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">

<meta name="Author" content="Abalone">
<meta name="Description" content="Abalone Beauty Cream ที่สุดของครีมยกกระชับ ตื่นมาใส ไม่ต้องรอ">
<meta property="og:title" content="Abalone">
<meta property="og:description" content="Abalone Beauty Cream ที่สุดของครีมยกกระชับ ตื่นมาใส ไม่ต้องรอ">
<meta property="og:image" content="<?php echo get_template_directory_uri(); ?>/img/abalone-logo.png">
<meta property="og:image:width" content="250">
<meta property="og:image:height" content="250">
<meta property="og:url" content="<?php get_site_url(); ?>">
<link rel="home" href="<?php get_site_url(); ?>">
<link rel="index" href="<?php get_site_url(); ?>/sitemap/">
<link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/img/favicon.ico">

<title><?php wp_title( '|', true, 'right' ); ?></title>
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="page" class="hfeed site off-canvas-wrap" data-offcanvas>
<div id="innerwrap" class="inner-wrap">
    <!-- Off Canvas Menu -->
    <aside class="right-off-canvas-menu">
		<?php wp_page_menu( array( 'menu_class' => 'header__nav' ) ); ?>
    </aside>
	<!-- close the off-canvas menu -->
	<a class="exit-off-canvas"></a>

	<!-- nav toggle -->
	<button class="header__toggle right-off-canvas-toggle visible-xs-block visible-sm-block pull-right">
		<span class="icon-bar"></span>
		<span class="icon-bar"></span>
		<span class="icon-bar"></span>
	</button>
	
	<header role="banner" class="header">
		<div class="container">
			

			<a href="<?php echo esc_url( home_url( '/' ) ); ?>" class="header__logo" rel="home">
				<!-- <img src="<?php echo get_template_directory_uri(); ?>/img/abalone-logo.png"> -->
				<img data-interchange="[<?php echo get_template_directory_uri(); ?>/img/abalone-logo-small.png, (small)], 
									   [<?php echo get_template_directory_uri(); ?>/img/abalone-logo-small.png, (medium)],
									   [<?php echo get_template_directory_uri(); ?>/img/abalone-logo.png, (large)]">
			</a>	
			<nav id="site-navigation" class="visible-md-block visible-lg-block">
				<?php wp_page_menu( array( 'menu_class' => 'header__nav' ) ); ?>
			</nav>
		</div> <!-- #container -->
	</header>

	<div id="content" class="site-content">
