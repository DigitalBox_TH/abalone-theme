<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package Abalone
 */
?>

	</div><!-- #content -->

	<footer role="contentinfo">
		<div class="container footer__overtab">
			<span class="footer__callcenter">Call Center :  095-606-0659</span>	
			<a class="footer__logo" href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home">
				<img src="<?php echo get_template_directory_uri(); ?>/img/footer-logo.png">
			</a>
		</div>
		<div class="footer__tabinfo">
			<div class="container">			
				<span>เพราะเราเข้าใจ.. ว่าอะไรที่ผิวคุณต้องการ</span>
				<span>www.abalonebeautycream.com</span>
 			</div>			
		</div>
	</footer><!-- #colophon -->

</div><!-- #innerwrap -->
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
